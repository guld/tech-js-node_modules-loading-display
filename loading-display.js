window.errorDisplay = {
  setNotLoading: function () {
    document.getElementById('loader-wait-msg').innerHTML = 'Please wait.'
    document.getElementById('loading-div').style.display = 'none'
  },
  setLoading: function (mess) {
    document.getElementById('loading-div').style.display = 'block'
    if (mess) {
      document.getElementById('loader-wait-msg').innerHTML = mess
    } else {
      document.getElementById('loader-wait-msg').innerHTML = 'Please wait.'
    }
  }
}

