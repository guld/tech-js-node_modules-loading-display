# loading-display

[![source](https://img.shields.io/badge/source-bitbucket-blue.svg)](https://bitbucket.org/guld/tech-js-node_modules-loading-display) [![issues](https://img.shields.io/badge/issues-bitbucket-yellow.svg)](https://bitbucket.org/guld/tech-js-node_modules-loading-display/issues) [![documentation](https://img.shields.io/badge/docs-guld.tech-green.svg)](https://guld.tech/lib/loading-display.html)

[![node package manager](https://img.shields.io/npm/v/loading-display.svg)](https://www.npmjs.com/package/loading-display) [![travis-ci](https://travis-ci.org/guldcoin/tech-js-node_modules-loading-display.svg)](https://travis-ci.org/guldcoin/tech-js-node_modules-loading-display?branch=guld) [![lgtm](https://img.shields.io/lgtm/grade/javascript/b/guld/tech-js-node_modules-loading-display.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/b/guld/tech-js-node_modules-loading-display/context:javascript) [![david-dm](https://david-dm.org/guldcoin/tech-js-node_modules-loading-display/status.svg)](https://david-dm.org/guldcoin/tech-js-node_modules-loading-display) [![david-dm](https://david-dm.org/guldcoin/tech-js-node_modules-loading-display/dev-status.svg)](https://david-dm.org/guldcoin/tech-js-node_modules-loading-display?type=dev)

Show a loading screen and hide on JS command.

### Install

##### Browser

```sh
curl https///bitbucket.org/guld/tech-js-node_modules-loading-display/raw/guld/loading-display.js -o loading-display.js
```


### License

MIT Copyright isysd
